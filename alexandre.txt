Jefferson Gomes da Silva
Senior Full Stack App Developer 

I’m a software engineer with more than 18 years of experience working as web developer, frontend and backend, and databases as well. 

I love to work as an instructor as I did many times during my career. I like taking care of my projects in all their different dimensions from requirement analysis, Interfacing with clients, communicating and also documenting everything. I’ve been working as a developer and also as a consultant.

I’m particularly interested in web technologies such as angular, ionic, react among others.


Main technologies I’ve used in my projects:

HTML/CSS/Bootstrap
Javascript / JQuery / Angular
Progress OpenEdge
C#.net
Python
PHP
Java
SQL Server
MySQL

Skills 
Progress OpenEdge
Javascript
Python

Idiomas:

Português, English, Spanish




- Progress Brazil - Consultant Software Engineer

Instructor / Webspeed

- Toyota Brasil - Replacement parts system 

- Software Engineer - ADP

Payment systems
System Mercosul

Webspeed Progress OpenEdge
HTML Javascript CSS


- HDI Seguros (Insurance Company) 2005-2007

Math and Calc Engine
Progress OpenEdge + Webspeed
HTML Javascript CSS



- Consultant Software Engineer Datasul ECM

Enterprise Content Management

Progress OpenEdge + Webspeed
HTML Javascript CSS



- Software Engineer - HTG

Progress OpenEdge
Reports
Process Analysis
UI/UX



- Consultant Software Engineer - Bull Data

6 months project

Dot net
C#
ASP net


- Software Engineer - TOTVS

UI/UX
In House instructor

FLUIG - Business Management Integration Tool

BPM design

HTML CSS Javascript Bootstrap
Progress OpenEdge
C#

Good Data - BI and Analytics

Cloud Integration for the tool 
Dashboards and Reports

FLUIG instructor
Good Data Instructor



